<?php

namespace App\Http\Controllers;

use App\Models\Patrons;
use Illuminate\Http\Request;
use App\Http\Requests\PatronsRequest;

class PatronsController extends Controller
{
    /**
     * DISPLAY
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patrons = Patrons::all();
        return response()->json([
            "message" => "List of Patrons",
            "data" => $patrons]);
    }

    
    /**
     * ADD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatronsRequest $request)
    {
        $patrons = new Patrons();

        $patrons->last_name = $request->last_name;
        $patrons->first_name = $request->first_name;
        $patrons->middle_name = $request->middle_name;
        $patrons->email = $request->email;

        $patrons->save($request->validated());
        return response()->json($patrons);
    }


    /**
     * SEARCH
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patrons = Patrons::find($id);
        return response()->json($patrons);
    }


    /**
     * UPDATE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatronsRequest $request, $id)
    {
        $patrons = Patrons::find($id);
        
        $patrons->last_name = $request->input('last_name');
        $patrons->first_name = $request->input('first_name');
        $patrons->middle_name = $request->input('middle_name');
        $patrons->email = $request->input('email');

        $patrons->update($request->validated());
        return response()->json($patrons);
    }

    
    /**
     * DELETE
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patrons = Patrons::find($id);
        $patrons->delete();
        return response()->json($patrons);
    }
}
