<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\PatronsController;
use App\Http\Controllers\Borrowed_BooksController;
use App\Http\Controllers\Returned_BooksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::Resources([

'categories' => CategoriesController::class,
'books' => BooksController::class,
'patrons' => PatronsController::class,
'borrowed_books' => Borrowed_BooksController::class,
'returned_books' => Returned_BooksController::class

]);