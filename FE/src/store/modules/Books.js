import axios from "axios";

const state = {
  Books: [],
};

const getters = {
  AllBooks: (state) => state.Books,
};

const actions = {
  async fetchBook({ commit }) {
    const response = await axios.get("http://127.0.0.1:8000/api/books");
    commit("Books", response.data.data);
  },

  async addBook({ commit }, Book) {
    const response = await axios.post(
      "http://127.0.0.1:8000/api/Books",
      Book
    );
    commit("NewBook", response.data);
  },

  async updateBook({ commit }, Book) {
    const response = await axios.put(
      `http://127.0.0.1:8000/api/books/${Book.id}`,
      Book
    );
    commit("EditBook", response.data);
  },

  async deleteBook({ commit }, Book) {
    axios.delete(`http://127.0.0.1:8000/api/books/${Book.id}`, Book);
    commit("DeleteBook", Book);
  },
};

const mutations = {
  Books: (state, Books) => (state.Books = Books),
  NewBook: (state, Book) => state.Books.unshift(Book),
  EditBook: (state, Book) => {
    const index = state.Books.findIndex((t) => t.id === Book.id);
    if (index !== -1) {
      state.Books.splice(index, 1, Book);
    }
  },
  DeleteBook: (state, Book) =>
    (state.Books = state.Books.filter((t) => Book.id !== t.id)),
};

export default {
  state,
  getters,
  actions,
  mutations,
};
