<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Borrowed_Books;
use App\Models\Returned_Books;
use App\Models\Books;
use App\Http\Requests\Borrowed_BooksRequest;

class Borrowed_BooksController extends Controller
{
    /**
     * DISPLAY
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrowedbooks = Borrowed_Books::all();
        return response()->json(["message" => "List of Borrowed Books",
        "data" => $borrowedbooks]);
        //
    }


    /**
     * ADD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Borrowed_BooksRequest $request)
    {
        $borrowedbooks = new Borrowed_Books();

        $borrowedbooks->copies = $request->copies;
        $borrowedbooks->book_id = $request->book_id;
        $borrowedbooks->patron_id = $request->patron_id;

        $books = Books::find($borrowedbooks->book_id);

        $subtractCopies = $books->copies - $borrowedbooks->copies;
        
        $borrowedbooks->save($request->validated());
        $books->update(['copies' => $subtractCopies]);
        return response()->json(
            ["message" => "Borrowed Successfully",
            "data" => $borrowedbooks, $books]
        );

        //
    }

    /**
     * SEARCH
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowedbooks = Borrowed_Books::find($id);
        return response()->json($borrowedbooks);
        //
    }


    /**
     * UPDATE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Borrowed_BooksRequest  $request, $id)
    {
        $borrowedbook = Borrowed_Books::find($id);
        $returnedbook = new Returned_Books();
        $returnedbook->copies = $request->copies;
        $returnedbook->book_id = $request->book_id;
        $returnedbook->patron_id = $request->patron_id;

        $books = Books::find($returnedbook->book_id);
        $addCopies = $books->copies + $returnedbook->copies;

        $returnedbook->save($request->validated());
        $borrowedbook->delete($request->validated());
        $books->update(['copies' => $addCopies]);
        return response()->json(["message" => "Success",
        "data" => $borrowedbook, $books, $returnedbook]);
        
        //
    }

}

