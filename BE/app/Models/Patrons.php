<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patrons extends Model
{
    use HasFactory;
    protected $table = 'patrons';
    protected  $fillable = ['last_name', 'first_name', 'middle_name', 'email'];


    public function borrowedBooks()
    {
        return $this->hasMany(Borrowed_Books::class, 'borrowed_books',  'patron_id');
    }

    public function returnedBooks()
    {
        return $this->hasMany(Returned_Books::class, 'returned_books', 'patron_id');
    }
    
}
